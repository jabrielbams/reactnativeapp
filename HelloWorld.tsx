import React, {Component} from 'react';
import {Image, Text, TextInput, View} from 'react-native';

const HelloWorld = () => {
  return (
    <View
      style={{
        flex: 1,
        margin: 25,
      }}>
      <Picture />
      <Text style={{fontSize: 20, marginTop: 20}}>Selamat Datang!</Text>
      <LoginForm />
    </View>
  );
};

// Functional Component
const Picture = () => {
  return (
    <Image
      source={{uri: 'https://placeimg.com/640/480/tech?t=1686294513406'}}
      style={{width: 300, height: 200}}
    />
  );
};

// Class Component
class LoginForm extends Component {
  render() {
    return (
      <View style={{}}>
        <View>
          <Text>Username</Text>
          <TextInput style={{borderWidth: 1}} />
        </View>
        <View>
          <Text>Password</Text>
          <TextInput style={{borderWidth: 1}} />
        </View>
      </View>
    );
  }
}

export default HelloWorld;
