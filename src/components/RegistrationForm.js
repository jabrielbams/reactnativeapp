import React, {useState} from 'react';
import {
  Alert,
  Button,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

const RegistrationScreen = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const HandleRegister = () => {
    // Validasi untuk memastikan tidak ada form yang kosong
    if (!name || !email || !password) {
      Alert.alert('Lengkapi semua field sebelum lanjut!');
      return;
    }
  };

  return (
    <View>
      <View style={styles.container}>
        <View>
          <Text style={{fontSize: 32, fontWeight: 'bold', color: '#171717'}}>
            Register
          </Text>
          <Text style={{fontSize: 16, color: '#999EA1'}}>
            Hai, Buat akun kamu disini
          </Text>
        </View>

        <View style={styles.mt32}>
          <View style={styles.mt16}>
            <Text style={{fontSize: 18, fontWeight: '500', color: '#171717'}}>
              Nama
            </Text>
            <TextInput
              style={styles.inputText}
              placeholder="Masukkan nama"
              value={name}
              onChangeText={Text => setName(Text)}
            />
          </View>

          <View style={styles.mt16}>
            <Text style={{fontSize: 18, fontWeight: '500', color: '#171717'}}>
              Email
            </Text>
            <TextInput
              style={styles.inputText}
              placeholder="Masukkan email"
              value={email}
              onChangeText={Text => setEmail(Text)}
            />
          </View>

          <View style={styles.mt16}>
            <Text style={{fontSize: 18, fontWeight: '500', color: '#171717'}}>
              Password
            </Text>
            <TextInput
              style={styles.inputText}
              placeholder="Masukkan password"
              value={password}
              secureTextEntry
              onChangeText={Text => setPassword(Text)}
            />
          </View>
        </View>

        <View style={styles.mt32}>
          <Pressable onPress={HandleRegister}>
            <View style={styles.registerBtn}>
              <Text style={{fontSize: 16, fontWeight: '500', color: '#F4F4F5'}}>
                Register
              </Text>
            </View>
          </Pressable>
          {/* <Button
          title="Masuk sekarang"
          onPress={() => Alert.alert('')}
        /> */}
        </View>

        <View
          style={{
            justifyContent: 'center',
            flexDirection: 'row',
            marginTop: 32,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '400',
              color: '#171717',
              marginEnd: 8,
            }}>
            Belum mempunyai akun?
          </Text>
          <Text style={{fontSize: 14, fontWeight: '600', color: '#3280EF'}}>
            Daftar
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 14,
    marginVertical: 64,
  },
  mt32: {
    marginTop: 32,
  },
  mt16: {
    marginTop: 16,
  },
  inputText: {
    marginVertical: 8,
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 12,
    paddingVertical: 16,
    fontSize: 16,
    borderColor: '#DDDDDD',
  },
  registerBtn: {
    height: 49,
    borderRadius: 8,
    backgroundColor: '#3280EF',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default RegistrationScreen;
