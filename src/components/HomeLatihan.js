import React, {useState} from 'react';
import {Alert, Text, View, StyleSheet, Image, Pressable} from 'react-native';

type userProps = {
  name: string,
};

const Greeting = (user: userProps) => {
  return (
    <View style={styles.header}>
      <View>
        <Text style={styles.greetingText}>Hello,</Text>
        <Text style={styles.greetingText}>{user.name}</Text>
      </View>
      <View style={styles.notifWrapper}>
        <Image
          style={styles.icon}
          source={require('../../assets/icon/notif.png')}
        />
        <Text style={styles.notif}>10</Text>
      </View>
    </View>
  );
};

const Button = () => {
  const [timesPressed, setTimesPressed] = useState(0);

  let textLog = '';
  if (timesPressed > 1) {
    textLog = timesPressed + '';
  } else if (timesPressed > 0) {
    textLog = '1';
  }

  return (
    <View style={{marginTop: 24}}>
      <Pressable
        onPress={() => {
          setTimesPressed(current => current + 1);
        }}>
        <View style={styles.button}>
          <Text style={{fontSize: 16, fontWeight: '500', color: '#F4F4F5'}}>
            Press me!
          </Text>
        </View>
      </Pressable>
      <Text>notif = {textLog}</Text>
    </View>
  );
};

const Home = () => {
  return (
    <View style={styles.container}>
      <Greeting name="Jabriel Bamasena" />
      <Button />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 14,
    marginVertical: 64,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  greetingText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#171717',
    marginBottom: 8,
  },
  notifWrapper: {
    borderWidth: 1,
    borderColor: '#DDDDDD',
    width: 52,
    height: 52,
    borderRadius: 52 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: 31,
    width: 31,
  },
  notif: {
    fontSize: 12,
    color: '#F4F4F5',
    backgroundColor: '#3280EF',
    alignItems: 'center',
    padding: 4,
    borderRadius: 14,
    width: 24,
    height: 24,
    position: 'absolute',
    top: -5,
    right: -5,
  },
  button: {
    height: 49,
    borderRadius: 8,
    backgroundColor: '#3280EF',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Home;
