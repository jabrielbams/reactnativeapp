import React from 'react';
import {
  Alert,
  Button,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

const LoginPage = () => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={{fontSize: 32, fontWeight: 'bold', color: '#171717'}}>
          Login
        </Text>
        <Text style={{fontSize: 16, color: '#999EA1'}}>
          Selamat datang kembali
        </Text>
      </View>

      <View style={{marginTop: 64}}>
        <View>
          <Text style={{fontSize: 18, fontWeight: '500', color: '#171717'}}>
            Email
          </Text>
          <TextInput
            style={styles.inputText}
            placeholder="Masukkan alamat email"
          />
        </View>
        <View style={{marginTop: 16}}>
          <Text style={{fontSize: 18, fontWeight: '500', color: '#171717'}}>
            Password
          </Text>
          <TextInput style={styles.inputText} placeholder="Masukkan password" />
        </View>
      </View>

      <View style={{marginTop: 64}}>
        <Pressable onPress={() => Alert.alert('Button login')}>
          <View style={styles.loginButton}>
            <Text style={{fontSize: 16, fontWeight: '500', color: '#F4F4F5'}}>
              Masuk Sekarang
            </Text>
          </View>
        </Pressable>
        {/* <Button
        title="Masuk sekarang"
        onPress={() => Alert.alert('')}
      /> */}
      </View>

      <View
        style={{
          justifyContent: 'center',
          flexDirection: 'row',
          marginTop: 126,
        }}>
        <Text
          style={{
            fontSize: 14,
            fontWeight: '400',
            color: '#171717',
            marginEnd: 8,
          }}>
          Belum mempunyai akun?
        </Text>
        <Text style={{fontSize: 14, fontWeight: '600', color: '#3280EF'}}>
          Daftar
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 14,
    marginVertical: 64,
  },
  inputText: {
    marginVertical: 8,
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 12,
    paddingVertical: 16,
    fontSize: 16,
    borderColor: '#DDDDDD',
  },
  loginButton: {
    height: 49,
    borderRadius: 8,
    backgroundColor: '#3280EF',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoginPage;